import React from 'react'

import ReactDOM from 'react-dom'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import {Redirect} from 'react-router'

import {Login} from './Pages/Login'
import {Home} from './Pages/Home'
import {Register} from './Pages/Register'
import {Pet} from './Pages/Pets/Pet'

const PrivateRoute = ({component: Component, state = {}, ...rest}) => (
    <Route {...rest}>
        {
            localStorage.getItem('auth_token') ? (
                <Component/>
            ) : (
                <Redirect to={{pathname: '/login', state}}/>
            )}
    </Route>
)

function Main() {
    return (
        <Router>
            <div>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <PrivateRoute exact path='/pet/:id' component={Pet}/>
                    <Route exact path='/login' component={Login} />
                    <Route exact path='/register' component={Register} />
                </Switch>
            </div>
        </Router>
    )
}

export default Main

if (document.getElementById('root')) {
    ReactDOM.render(<Main/>, document.getElementById('root'))
}
