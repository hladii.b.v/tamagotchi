import {BASE_URL} from './env'

const TYPE_CAT     = 'cat'
const TYPE_DOG     = 'dog'
const TYPE_RACCOON = 'raccoon'
const TYPE_PENGUIN = 'penguin'

const FEATURE_HUNGER = 'hunger'
const FEATURE_DREAM  = 'dream'
const FEATURE_CARE   = 'care'


export const PET_IMAGES = {
    [TYPE_CAT]: `${BASE_URL}/images/cat.png`,
    [TYPE_DOG]: `${BASE_URL}/images/dog.png`,
    [TYPE_RACCOON]: `${BASE_URL}/images/raccoon.png`,
    [TYPE_PENGUIN]: `${BASE_URL}/images/penguin.png`
}

export const FEATURE_IMAGES = {
    [FEATURE_HUNGER]: `${BASE_URL}/images/hunger.jpg`,
    [FEATURE_DREAM]: `${BASE_URL}/images/dream.png`,
    [FEATURE_CARE]: `${BASE_URL}/images/care.png`
}
