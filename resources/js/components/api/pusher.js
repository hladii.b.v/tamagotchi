import Pusher from 'pusher-js'

export const initPusher = () => {
    return new Pusher(
        '146d98d641c89cb7a264',
        {
            cluster: 'eu'
        }
    )
}
