import axios from 'axios'

import {BASE_URL} from '../helper/env'

axios.defaults.baseURL = `${BASE_URL}/api/v1`

axios.defaults.headers.Authorization = `Bearer ${localStorage.getItem(
    'auth_token'
)}`

export const registerRequest = (data) => {
    return axios.post('/users',  data)
}

export const loginRequest = (auth) => {
    return axios.post('/login', [], { auth })
}

export const fetchUser = () => {
    return axios.get('/users')
}

export const fetchPetsRequest = () => {
    return axios.get('/pets')
}

export const createPetRequest = (data) => {
    return axios.post('/pets', data)
}

export const readPetRequest = (id) => {
    return axios.get(`/pets/${id}`)
}

export const updatePetRequest = (petId, featureId) => {
    return axios.put(`/pets/${petId}/${featureId}`)
}

export const deletePetRequest = (id) => {
    return axios.delete(`/pets/${id}`)
}

export const fetchPetTypesRequest = () => {
    return axios.get('/meta/types')
}

export const getFeaturesRequest = () => {
    return axios.get('/meta/features')
}
