import React, {useState, useEffect} from 'react'

import Loader from 'react-loader-spinner'
import {Link, useHistory} from 'react-router-dom'

import {deletePetRequest, fetchPetsRequest, fetchPetTypesRequest, fetchUser} from '../api/api'
import {CreateForm} from './Pets/CreateForm'

import {BsFillTrashFill} from 'react-icons/bs'

export const Pets = () => {
    const [pets, setPets] = useState([])
    const [types, setTypes] = useState([])
    const [user, setUser] = useState(null)
    const [loading, setLoading] = useState(true)
    const history = useHistory()

    useEffect(() => {
        fetchData()
    }, [])

    useEffect(() => {
        if (user) {
            setLoading(false)
        }
    }, [user, pets, types])

    async function fetchData() {
        const { data } = await fetchUser()

        if (data.success) {
            setUser(data.user)
        }

        await fetchPets()
        await fetchTypes()
    }

    async function fetchPets() {
        const { data } = await fetchPetsRequest()

        if (data.pets) {
            setPets(data.pets)
        }
    }

    const fetchTypes = async () => {
        const {data} = await fetchPetTypesRequest()

        if (data.success) {
            setTypes(data.types)
        }
    }

    const deletePet = async (id) => {
        const {data} = await deletePetRequest(id)

        if (data.success) {
            setPets(pets.filter((item) => item.id !== id))
        }
    }

    const getTypeName = (id) => {
        const type = types.find((type) => type.id === id)

        if (type) {
            return type.name
        } else {
            return 'Wrong type'
        }
    }

    const logOut = () => {
        history.push('/login')
        localStorage.removeItem('auth_token')
    }

    if (loading) return <Loader
        type='Circles'
        color='#00BFFF'
        height={100}
        width={100}
    />

    return user && <div className='container'>
        <div className='row mt-5 justify-content-center'>
            <div style={{display: 'flex', justifyContent: 'space-between', width: '100%'}}>
                <h3>Hello, {user.name || 'No name'}</h3>
                <button type='button' className='btn btn-secondary' onClick={() => logOut()}>Log out</button>
            </div>
        </div>
        {
            pets.length < types.length && <div className='row mt-5 justify-content-center'>
                <CreateForm
                    initPets={pets}
                    onUpdate={(pet) => setPets([...pets, pet])}
                    types={types}
                />
            </div>
        }
        <div className='row mt-5 justify-content-center'>
            {
                pets.length === 0 && <div className='col-md-6 text-center'>
                    <h3>No pets</h3>
                </div>
            }
            {
                pets.length > 0 && <div className='col-md-6 text-center'>
                    <table className='table table-striped'>
                        <thead>
                        <tr>
                            <th scope='col'>Name</th>
                            <th scope='col'>Type</th>
                            <th scope='col'></th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            pets.map((pet) =>
                                <tr key={pet.id} className={pet.is_dead && 'table-danger'}>
                                    <td>
                                        <Link
                                            to={{pathname: `/pet/${pet.id}`}}
                                        >
                                            {pet.name}
                                        </Link>
                                    </td>
                                    <td>{getTypeName(pet.pet_type_id)}</td>
                                    <td>
                                        <button onClick={() => deletePet(pet.id)} type='button'
                                                className='btn btn-secondary'>
                                            <BsFillTrashFill/>
                                        </button>
                                    </td>
                                </tr>
                            )
                        }
                        </tbody>

                    </table>
                </div>
            }
        </div>
    </div>
}
