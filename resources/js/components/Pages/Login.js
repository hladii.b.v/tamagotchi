import React, {useState, useEffect} from 'react'

import {useHistory} from 'react-router-dom'

import {loginRequest} from '../api/api'
import axios from "axios";

export const Login = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState({})
    let history = useHistory()

    useEffect(() => {
        if (localStorage.getItem('auth_token')) {
            history.push('/')
        }
    }, [])

    useEffect(() => {
        if (Object.keys(error).length > 0) {
            validate()
        }
    }, [email, password])

    const createUser = async () => {
        if (Object.keys(validate()).length === 0) {
            const {data} = await loginRequest({username: email, password})

            if (data.success) {
                const {api_token: token} = data.user

                localStorage.setItem('auth_token', token)
                axios.defaults.headers.Authorization = `Bearer ${token}`
                history.push('/')
            } else {
                setError({...error, login: 'Something went wrong'})
            }
        }
    }

    const validate = () => {
        const errors = {}
        const emailRegex = /^(([^<>()[\]\\.,:\s@]+(\.[^<>()[\]\\.,:\s@]+)*)|(.+))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        if (!emailRegex.test(email)) {
            errors.email = 'Wrong email'
        }
        if (password.length === 0) {
            errors.password = 'Empty password'
        }

        setError(errors)
        return errors
    }

    return <div className='container'>
        <div className='row justify-content-center'>
            <div className='col-md-8'>
                <div className='card'>
                    <div className='card-header'>Login</div>
                    <div className='card-body'>
                        <div className='form-group row'>
                            <label className='col-md-4 col-form-label text-md-right'>E-Mail Address</label>
                            <div className='col-md-6'>
                                <input onChange={({target}) => setEmail(target.value)} className='form-control'/>
                                {
                                    error.hasOwnProperty('email') && <span className='invalid-feedback d-block'>
                                        {error.email}
                                    </span>
                                }
                            </div>
                        </div>
                        <div className='form-group row'>
                            <label className='col-md-4 col-form-label text-md-right'>Password</label>
                            <div className='col-md-6'>
                                <input onChange={({target}) => setPassword(target.value)} className='form-control'/>
                                {
                                    error.hasOwnProperty('password') && <span className='invalid-feedback d-block'>
                                        {error.password}
                                    </span>
                                }
                            </div>
                        </div>
                        <div className='form-group row mb-0'>
                            <div style={{display: 'flex', justifyContent: 'space-between', width: '100%', padding: '20px'}}>
                                <button type='submit' onClick={() => createUser()} className='btn btn-primary'>
                                    Login
                                </button>

                                <button onClick={() => history.push('/register')} className='btn btn-secondary'>
                                    Register
                                </button>
                            </div>
                            <div style={{display: 'flex', justifyContent: 'space-between', width: '100%', padding: '20px'}}>
                                {
                                    error.hasOwnProperty('login') && <span className='invalid-feedback d-block'>
                                        {error.login}
                                    </span>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}
