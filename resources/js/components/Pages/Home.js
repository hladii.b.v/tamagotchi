import React, {useState, useEffect} from 'react'

import {useHistory} from 'react-router-dom'

import {Pets} from './Pets'

export const Home = () => {
    const [isUser, setIsUser] = useState(false)
    let history = useHistory()

    useEffect(() => {
        if (localStorage.getItem('auth_token')) {
            setIsUser(true)
        }
    }, [])

    return <div className='container'>
        {
            !isUser && <div className='mt-5' style={{display: 'flex', justifyContent: 'space-between', width: '100%'}}>
                <div className='col-md-6 text-center'>
                    <button onClick={() => history.push('/login')} className='btn btn-primary'>
                        Login
                    </button>
                </div>
                <div className='col-md-6 text-center'>
                    <button onClick={() => history.push('/register')} className='btn btn-primary'>
                        Register
                    </button>
                </div>
            </div>
        }
        {
            isUser && <Pets/>
        }
    </div>
}
