import React, {useEffect, useState} from 'react'

import Loader from 'react-loader-spinner'

import {createPetRequest} from '../../api/api'

export const CreateForm = ({initPets = [], types, onUpdate}) => {
    const [name, setName] = useState('')
    const [type, setType] = useState(null)
    const [filteredTypes, setFilteredTypes] = useState([])
    const [error, setError] = useState({})
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if (Object.keys(error).length > 0) {
            validate()
        }
    }, [name, type])

    useEffect(() => {
        setType(null)
    }, [initPets])

    useEffect(() => {
        setFilteredTypes(
            types
                .filter((type) => !initPets.some((pet) => pet.pet_type_id === type.id))
        )
    }, [initPets])

    const createPet = async () => {
        if (Object.keys(validate()).length === 0) {
            setLoading(true)
            const {data} = await createPetRequest({name, type})

            if (data.success) {
                const {pet} = data

                onUpdate(pet)
            } else {
                setError({...error, create: 'Something went wrong'})
            }

            setLoading(false)
        }
    }

    const validate = () => {
        const errors = {}

        if (name.length === 0) {
            errors.name = 'Empty name'
        }

        if (!type) {
            errors.type = 'Empty type'
        }

        setError(errors)

        return errors
    }

    if (loading) return <Loader
        type='Circles'
        color='#00BFFF'
        height={100}
        width={100}
    />

    return <div className='col-md-8'>
        <div className='card'>
            <div className='card-body'>
                <div className='form-group row'>
                    <label className='col-md-4 col-form-label text-md-right'>Name</label>
                    <div className='col-md-6'>
                        <input onChange={({target}) => setName(target.value)} className='form-control'/>
                        {
                            error.hasOwnProperty('name') && <span className='invalid-feedback d-block'>
                                {error.name}
                            </span>
                        }
                    </div>
                </div>
                <div className='form-group row'>
                    <label className='col-md-4 col-form-label text-md-right'>Type</label>
                    <div className='col-md-6'>
                        <div onChange={({target}) => setType(parseInt(target.value))}>
                            {
                                filteredTypes.length > 0  && filteredTypes
                                    .filter((type) => !initPets.some((pet) => pet.pet_type_id === type.id))
                                    .map((type, index) =>
                                        <div className="form-check">
                                            <input
                                                key={index}
                                                type='radio'
                                                value={type.id}
                                                className='form-check-input'
                                                id={`flexRadioDefault${type.id}`}
                                            />
                                            <label
                                                className='form-check-label'
                                                htmlFor={`flexRadioDefault${type.id}`}
                                            >
                                                {type.name}
                                            </label>
                                        </div>
                                    )
                            }
                        </div>
                        {
                            error.hasOwnProperty('type') && <span className='invalid-feedback d-block'>
                                {error.type}
                            </span>
                        }
                    </div>
                </div>
                <div className='form-group row mb-0'>
                    <div className='col-md-8 offset-md-4'>
                        <button type='submit' onClick={() => createPet()} className='btn btn-primary'>
                            Create
                        </button>
                    </div>
                    <div className='col-md-8 offset-md-4'>
                        {
                            error.hasOwnProperty('create') && <span className='invalid-feedback d-block'>
                                {error.create}
                            </span>
                        }
                    </div>
                </div>
            </div>
        </div>
    </div>
}
