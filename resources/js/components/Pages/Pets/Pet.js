import React, {useEffect, useState} from 'react'

import {useHistory, useParams} from 'react-router-dom'
import Loader from 'react-loader-spinner'

import {initPusher} from '../../api/pusher'
import {getFeaturesRequest, updatePetRequest, readPetRequest} from '../../api/api'
import { PET_IMAGES, FEATURE_IMAGES } from '../../helper/pets'

export const Pet = () => {
    const [pet, setPet] = useState(null)
    const [features, setFeatures] = useState([])
    const [loading, setLoading] = useState(true)
    const history = useHistory()
    const {id} = useParams()

    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = async () => {
        await fetchPet()
        await fetchFeature()
        setLoading(false)
    }

    const fetchPet = async () => {
        const {data} = await readPetRequest(id)

        if (data.success) {
            connectChanel(data.pet.id)
            setPet(data.pet)
        }
    }

    const fetchFeature = async () => {
        const {data} = await getFeaturesRequest()

        if (data.features) {
            setFeatures(data.features)
        }
    }

    const connectChanel = (id) => {
        initPusher().subscribe(`pet.${id}`)
            .bind('update-feature', function (data) {
                if (data) {
                    setPet(data)
                }
            })
    }

    const updateFeature = async ($petId, featureId) => {
        const {data} = await updatePetRequest($petId, featureId)

        if (data.success) {
            setPet(data.pet)
        }
    }

    const getFeatureValue = (id, features) => {
        const feature = features.find(({feature_id}) => feature_id === id)

        if (feature) {
            return feature.value
        } else {
            return 'No value'
        }
    }

    const goBack = () => {
        if (pet) {
            initPusher().unsubscribe(`pet.${id}`)
        }
        history.push('/')
    }

    if (loading) return <Loader
        type='Circles'
        color='#00BFFF'
        height={100}
        width={100}
    />

    if (pet.is_dead) return <div className='col-md-8'>
        <button type='button' className='btn btn-secondary' onClick={() => goBack()}>Go back</button>
        <h3>
            Pet is dead
        </h3>
    </div>

    return <div className='container'>
        <div className='row mt-5 justify-content-center'>
            <div className='col-md-8'>
                <button type='button' className='btn btn-secondary' onClick={() => goBack()}>Go back</button>
            </div>
            <div className='col-md-8 text-center'>
                <table style={{width: '100%', marginTop: '30px'}}>
                    <thead>
                    <tr>
                        {
                            features.map((feature) =>
                                <th key={feature.id} scope='col'>
                                    <img src={`${FEATURE_IMAGES[feature.type.name]}`} alt={feature.type.name} width='30px' />
                                </th>
                            )
                        }
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        {
                            features.map(({id}) =>
                                <td key={id}>
                                    <div>
                                        {getFeatureValue(id, pet.features)}
                                    </div>
                                </td>
                            )
                        }
                    </tr>
                    </tbody>
                </table>
                <h3>{pet.name}</h3>
            </div>
            <div className='col-md-8 text-center'>
                <img src={`${PET_IMAGES[pet.type.name]}`} alt={pet.type.name} width='200px' height='200px'/>
            </div>
            <div style={{display: 'flex', justifyContent: 'space-between', width: '100%'}}>
                {
                    features.map((feature) =>
                        <div
                            key={feature.id}
                        >
                            <button
                                onClick={() => updateFeature(pet.id, feature.id)}
                                className='btn btn-primary'
                            >
                                add {feature.type.name}
                            </button>
                        </div>
                    )
                }
            </div>
        </div>
    </div>
}
