<?php

namespace App\Events;

use App\Http\Resources\PetResource;
use App\Models\Pet;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class UpdatePetEvent
 * @package App\Events
 */
class UpdatePetEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Pet
     */
    public Pet $pet;

    /**
     * @param Pet $pet
     */
    public function __construct(Pet $pet)
    {
        $this->pet = $pet;

        $this->pet->load(
            [
                'features',
                'type'
            ]
        );
    }

    /**
     * @return string[]
     */
    public function broadcastOn(): array
    {
        return ['pet.' . $this->pet->id];
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return PetResource::make($this->pet)
            ->toArray(app('request'));
    }

    /**
     * @return string
     */
    public function broadcastAs(): string
    {
        return 'update-feature';
    }
}
