<?php

namespace App\Http\Controllers;

use App\Http\Resources\JsonResource;
use App\Models\Feature;
use App\Models\PetType;

/**
 * Class PetMetaController
 * @package App\Http\Controllers
 */
class PetMetaController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function features()
    {
        return JsonResource::create(
            Feature::query()->with('type')->get(),
            'features'
        );
    }

    /**
     * @return JsonResource
     */
    public function types()
    {
        return JsonResource::create(
            PetType::query()->get(),
            'types'
        );
    }
}
