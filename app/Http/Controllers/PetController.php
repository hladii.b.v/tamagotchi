<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePetRequest;
use App\Http\Resources\PetResource;
use App\Jobs\PetFeatures;
use App\Models\Feature;
use App\Models\Pet;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class PetController
 * @package App\Http\Controllers
 */
class PetController extends Controller
{
    /**
     * @return JsonResource
     */
    public function index()
    {
        return PetResource::collection(Auth::user()->pets()->get());
    }

    /**
     * @param CreatePetRequest $request
     * @return PetResource
     */
    public function create(CreatePetRequest $request)
    {
        DB::beginTransaction();

        /** @var Pet $pet */
        $pet = Auth::user()
            ->pets()
            ->create(
                [
                    'name' => $request->get('name'),
                    'pet_type_id' => $request->get('type')
                ]
            );

        Feature::query()->each(
            function ($feature) use ($pet) {
                $pet->features()->create(
                    [
                        'pet_id' => $pet->id,
                        'feature_id' => $feature->id,
                        'decline' => Carbon::now(),
                        'increase' => Carbon::now()
                    ]
                );
            }
        );

        if (!$pet->features()->exists()) {
            DB::rollback();
        } else {
            DB::commit();
        }

        return new PetResource($pet->load('features'));
    }

    /**
     * @param int $id
     * @return PetResource
     */
    public function read(int $id)
    {
        return new PetResource(
            Pet::query()->findOrFail($id)->load('features', 'type')
        );
    }

    /**
     * @param int $id
     * @param int $featureId
     * @return PetResource
     */
    public function update(int $id, int $featureId)
    {
        return new PetResource(
            dispatch_now(new PetFeatures($id, $featureId))->load('features', 'type')
        );
    }

    /**
     * @param int $id
     * @return PetResource
     * @throws Exception
     */
    public function delete(int $id)
    {
        /** @var Pet $pet */
        $pet = Pet::query()->findOrFail($id);

        $pet->features()->delete();
        $pet->delete();

        return new PetResource($pet);
    }
}
