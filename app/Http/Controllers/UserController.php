<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @return UserResource
     */
    public function readByAuth()
    {
        return new UserResource(Auth::user());
    }

    /**
     * @return UserResource
     */
    public function read()
    {
        $user = Auth::user();

        return new UserResource($user);
    }

    /**
     * @param CreateUserRequest $request
     * @return UserResource
     */
    public function create(CreateUserRequest $request)
    {
        $user = User::create(
            $request->onlyWhitelist() + [
                'password' => Hash::make($request->get('password'))
            ]
        );

        return new UserResource($user);
    }
}
