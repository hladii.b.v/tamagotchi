<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * Class CreatePetRequest
 * @package App\Http\Requests
 */
class CreatePetRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'type' => [
                'required',
                'int',
                Rule::unique('pets', 'pet_type_id')->ignore(Auth::user(), 'user_id')
            ]
        ];
    }
}
