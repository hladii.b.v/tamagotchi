<?php

namespace App\Http\Requests;

/**
 * Class CreateUserRequest
 * @package App\Http\Requests
 */
class CreateUserRequest extends FormRequest
{
    public function rules ()
    {
        return [
            'email'     => 'required|email|max:255',
            'name'      => 'required|string|max:255',
            'password'  => 'required|string|max:255',
        ];
    }

    public function blacklist()
    {
        return [
            'password'
        ];
    }
}
