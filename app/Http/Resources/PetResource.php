<?php

namespace App\Http\Resources;

use App\Models\Pet;
use Illuminate\Http\Request;

/**
 * Class PetResource
 * @package App\Http\Resources
 * @property Pet $resource
 */
class PetResource extends JsonResource
{
    /**
     * @var string
     */
    public static $wrap = 'pet';

    /**
     * @var string
     */
    public static $collectionWrap = 'pets';

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'          => $this->resource->id,
            'name'        => $this->resource->name,
            'pet_type_id' => $this->resource->pet_type_id,
            'is_dead'     => $this->resource->is_dead,
        ];


        if ($this->resource->relationLoaded('features')) {
            $data['features'] = $this->resource->features;
        }

        if ($this->resource->relationLoaded('type')) {
            $data['type'] = $this->resource->type;
        }

        return $data;
    }
}
