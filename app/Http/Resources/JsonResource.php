<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource as IlluminateJsonResource;

/**
 * Class JsonResource
 * @package App\Http\Resources
 */
class JsonResource extends IlluminateJsonResource
{
    /**
     * @var string
     */
    public static $collectionWrap = 'data';

    /**
     * @var bool
     */
    public $inCollection = false;

    /**
     * @var array
     */
    protected $data = [
        //
    ];

    /**
     * @param mixed $resource
     * @param bool $silent
     * @return IlluminateJsonResource
     */
    public static function collection($resource, bool $silent = false)
    {
        if (static::$collectionWrap !== null && $silent === false) {
            IlluminateJsonResource::wrap(static::$collectionWrap);
        }

        return new IlluminateJsonResource($resource, static::class);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (is_null($this->resource)) {
            return [];
        }

        return is_array($this->resource) || $this->resource instanceof \stdClass
            ? $this->resource
            : $this->resource->toArray();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request)
    {
        $response = parent::toResponse($request);

        $this->additional(
            [
                'status'  => $response->getStatusCode(),
                'success' => $this->additional['success'] ?? $response->isSuccessful(),
            ]
        );

        return parent::toResponse($request);
    }

    /**
     * @inheritDoc
     */
    public function resolve($request = null)
    {
        if ($this->resource instanceof \stdClass) {
            return $this->resource;
        }

        return parent::resolve($request);
    }

    /**
     * @return bool
     */
    public function isInCollection(): bool
    {
        return $this->inCollection;
    }

    /**
     * @param bool $inCollection
     */
    public function setInCollection(bool $inCollection): void
    {
        $this->inCollection = $inCollection;
    }

    /**
     * @param $data
     * @param null $wrap
     * @return JsonResource
     */
    public static function create($data, $wrap = null)
    {
        static::$wrap = $wrap;
        return static::make($data);
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }
}
