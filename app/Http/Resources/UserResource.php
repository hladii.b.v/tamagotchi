<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @property User $resource
 */
class UserResource extends JsonResource
{
    /**
     * @var string
     */
    public static $wrap = 'user';

    /**
     * @var string
     */
    public static $collectionWrap = 'users';

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'        => $this->resource->id,
            'name'      => $this->resource->name,
            'email'     => $this->resource->email,
            'api_token' => $this->resource->api_token
        ];

        return $data;
    }
}
