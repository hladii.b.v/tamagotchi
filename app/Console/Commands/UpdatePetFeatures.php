<?php

namespace App\Console\Commands;

use App\Events\UpdatePetEvent;
use App\Models\FeatureType;
use App\Models\Pet;
use App\Models\PetFeature;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class UpdatePetFeatures
 * @package App\Console\Commands
 */
class UpdatePetFeatures extends Command
{
    /**
     * @var string
     */
    protected $signature = 'pet:features:update';

    /**
     * @var Pet
     */
    protected Pet $pet;

    /**
     * @var bool
     */
    protected bool $updatePet;

    /**
     * Handle command execution
     * @return void
     */
    public function handle()
    {
        foreach (Pet::query()->where('is_dead', false)->cursor() as $pet) {
            $this->pet = $this->killPet($pet);
            $this->updatePet = false;

            if ($this->pet->is_dead === true) {
                continue;
            }

            $this->pet->features->each(
                function ($petFeature) {
                    $this->updateValues($petFeature);

                    if ($petFeature->wasChanged() && $this->updatePet === false) {
                        $this->updatePet = true;
                    }
                }
            );

            if ($this->updatePet === true) {
                event(new UpdatePetEvent($this->pet));
            }
        }
    }

    /**
     * @param Pet $pet
     * @return Pet
     */
    protected function killPet(Pet $pet): Pet
    {
        if ($pet->dead_at !== null && $pet->dead_at <= Carbon::now()) {
            $pet->update(
                [
                    'is_dead' => true
                ]
            );
        }

        return $pet;
    }

    protected function updateValues(PetFeature $petFeature)
    {
        $feature = $petFeature->feature;
        $value = $petFeature->value;
        $declineTime = $feature->decline_time;

        if (isset($feature->rule['condition'])) {
            $condition = $feature->rule['condition'];

            switch ($feature->type->name) {
                case FeatureType::TYPE_HUNGER:
                    if ($condition['value'] >= $value && $this->pet->dead_at === null) {
                        $this->pet->update(
                            [
                                'dead_at' => Carbon::now()->addSeconds($condition['time'])
                            ]
                        );
                    }
                case FeatureType::TYPE_CARE:
                    /** @var PetFeature $dream */
                    $dream = $this->pet
                        ->features()
                        ->where(
                            function ($query) {
                                return $query->whereHas('feature', function ($query) {
                                    return $query->whereHas('type', function ($query) {
                                        return $query->where('name', FeatureType::TYPE_DREAM);
                                    });
                                });
                            }
                        )->firstOrFail();

                    if ($condition['value'] > $dream->value) {
                        $declineTime = $declineTime / 3;
                    }
            }
        }

        if ($value > 0 && $petFeature->decline <= Carbon::now()->subMinutes($declineTime)) {
            $petFeature->update(
                [
                    'decline' => Carbon::now(),
                    'value' => ($value - 1)
                ]
            );
        }
    }
}
