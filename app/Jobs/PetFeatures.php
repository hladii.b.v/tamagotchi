<?php

namespace App\Jobs;

use App\Models\Feature;
use App\Models\FeatureType;
use App\Models\Pet;
use App\Models\PetFeature;
use Carbon\Carbon;

/**
 * Class PetFeatures
 */
class PetFeatures
{
    /**
     * @var int
     */
    protected int $petId;

    /**
     * @var int
     */
    protected int $featureId;

    /**
     * SendCampaign constructor.
     * @param int $petId
     * @param int $featureId
     */
    public function __construct(int $petId, int $featureId)
    {
        $this->petId = $petId;
        $this->featureId = $featureId;
    }

    /**
     * Handle job call
     */
    public function handle(): Pet
    {
        $petFeature = PetFeature::query()
            ->where('pet_id', $this->petId)
            ->where('feature_id', $this->featureId)
            ->firstOrFail();

        /** @var Feature $feature */
        $feature = $petFeature->feature;

        /** @var Pet $pet */
        $pet = $petFeature->pet;

        if ($petFeature->increase <= Carbon::now()->subMinutes($feature->increase_time) && $petFeature->value < 100) {
            $petFeature->update(
                [
                    'increase' => Carbon::now(),
                    'value' => ($petFeature->value + 1)
                ]
            );
        }

        if ($feature->type->name === FeatureType::TYPE_HUNGER && $petFeature->value > 5) {
            $pet->update(
                [
                    'dead_at' => null
                ]
            );
        }

        return $pet;
    }
}
