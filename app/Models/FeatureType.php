<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class FeatureType
 * @property User $user
 * @property string $name
 * @package App\Models
 */
class FeatureType extends Model
{
    const TYPE_HUNGER = 'hunger';
    const TYPE_DREAM = 'dream';
    const TYPE_CARE = 'care';

    protected $fillable = [
        'name'
    ];

    /**
     * @return HasMany
     */
    public function pets(): HasMany
    {
        return $this->hasMany(Feature::class);
    }
}
