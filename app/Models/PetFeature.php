<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class PetFeature
 * @property Pet $pet
 * @property int $value
 * @property Carbon $decline
 * @property Feature $feature
 * @package App\Models
 */
class PetFeature extends Model
{
    protected $table = 'pet_feature';

    protected $fillable = [
        'pet_id',
        'feature_id',
        'value',
        'decline',
        'increase'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'decline',
        'increase',
    ];


    /**
     * @return BelongsTo
     */
    public function pet(): BelongsTo
    {
        return $this->belongsTo(Pet::class, 'pet_id');
    }

    /**
     * @return BelongsTo
     */
    public function feature(): BelongsTo
    {
        return $this->belongsTo(Feature::class);
    }
}
