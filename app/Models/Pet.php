<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Pet
 * @property User $user
 * @property PetType $type
 * @property PetFeature $features
 * @property boolean $is_dead
 * @property Carbon $dead_at
 * @package App\Models
 */
class Pet extends Model
{
    protected $fillable = [
        'name',
        'pet_type_id',
        'is_dead',
        'dead_at'
    ];

    protected $casts = [
        'is_dead' => 'bool'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'dead_at',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(PetType::class, 'pet_type_id');
    }

    /**
     * @return HasMany
     */
    public function features(): HasMany
    {
        return $this->hasMany(PetFeature::class);
    }
}
