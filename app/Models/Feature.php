<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Feature
 * @property User $user
 * @property FeatureType $type
 * @property int $decline_time
 * @package App\Models
 */
class Feature extends Model
{
    protected $fillable = [
        'feature_type_id',
        'decline_time',
        'increase_time',
        'rule'
    ];

    protected $casts = [
        'rule' => 'array'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(FeatureType::class, 'feature_type_id');
    }

    /**
     * @return HasMany
     */
    public function pets(): HasMany
    {
        return $this->hasMany(PetFeature::class);
    }
}
