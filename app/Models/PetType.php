<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class PetType
 * @property User $user
 * @package App\Models
 */
class PetType extends Model
{
    const TYPE_CAT = 'cat';
    const TYPE_DOG = 'dog';
    const TYPE_RACCOON = 'raccoon';
    const TYPE_PENGUIN = 'penguin';

    protected $table = 'pet_types';

    protected $fillable = [
        'name'
    ];

    /**
     * @return HasMany
     */
    public function pets(): HasMany
    {
        return $this->hasMany(Pet::class);
    }
}
