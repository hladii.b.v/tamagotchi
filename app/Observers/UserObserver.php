<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Str;

class UserObserver
{
    /**
     * @param User $user
     */
    public function creating(User $user)
    {
        if (!$user->api_token) {
            $user->setAttribute('api_token', Str::random(60));
        }
    }
}
