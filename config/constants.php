<?php

return [
    'feature' => [
        'types' => [
            \App\Models\FeatureType::TYPE_CARE,
            \App\Models\FeatureType::TYPE_DREAM,
            \App\Models\FeatureType::TYPE_HUNGER
        ],
        'time_value' => [
            \App\Models\FeatureType::TYPE_CARE => [
                'decline_time'  => 15,
                'increase_time' => 5
            ],
            \App\Models\FeatureType::TYPE_DREAM => [
                'decline_time'  => 20,
                'increase_time' => 10
            ],
            \App\Models\FeatureType::TYPE_HUNGER => [
                'decline_time' => 10,
                'increase_time' => 5
            ]
        ],
        'rule' => [
            \App\Models\FeatureType::TYPE_HUNGER => [
                'condition' => [
                    'value' => 5,
                    'time' => 3600
                ]
            ],
            \App\Models\FeatureType::TYPE_CARE => [
                'condition' => [
                    'value' => 5
                ]
            ]
        ]
    ],
    'pet' => [
        'types' => [
            \App\Models\PetType::TYPE_CAT,
            \App\Models\PetType::TYPE_DOG,
            \App\Models\PetType::TYPE_PENGUIN,
            \App\Models\PetType::TYPE_RACCOON
        ]
    ]
];
