<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateFeatureTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_types', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
        });

        array_map(
            function ($type) {
                DB::table('feature_types')
                    ->insert(
                        [
                            'name' => $type
                        ]
                    );
            },
            config('constants.feature.types')
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_types');
    }
}
