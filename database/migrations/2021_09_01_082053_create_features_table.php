<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('feature_type_id')->unique();
            $table->integer('decline_time');
            $table->integer('increase_time');
            $table->json('rule')->nullable();
        });

        foreach (DB::table('feature_types')->cursor() as $type) {
            DB::table('features')
                ->insert(
                    [
                        'feature_type_id' => $type->id,
                        'decline_time'    => config("constants.feature.time_value.{$type->name}.decline_time"),
                        'increase_time'   => config("constants.feature.time_value.{$type->name}.increase_time"),
                        'rule'            => json_encode(config("constants.feature.rule.{$type->name}"))
                    ]
                );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
    }
}
