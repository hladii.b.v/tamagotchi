<?php

use App\Http\Middleware\BasicMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::group(['middleware' => BasicMiddleware::class], function () {

        Route::post('/login', 'UserController@readByAuth');

    });

    Route::group(['prefix' => 'users'], function () {

        Route::post('/', 'UserController@create')->name('create.user');

        Route::group(['middleware' => 'api-auth'], function () {

            Route::get('/', 'UserController@read')->name('get.user');

        });

    });

    Route::group(['prefix' => 'pets'], function () {

        Route::group(['middleware' => 'api-auth'], function () {

            Route::get('/', 'PetController@index')->name('get.pets');

            Route::post('/', 'PetController@create')->name('create.pet');

            Route::group(['prefix' => '{id}'], function () {

                Route::get('/', 'PetController@read')->name('get.pet');

                Route::put('/{featureId}', 'PetController@update')->name('update.pet');

                Route::delete('/', 'PetController@delete')->name('delete.pet');

            });

        });

    });

    Route::group(['prefix' => 'meta'], function () {

        Route::group(['middleware' => 'api-auth'], function () {

            Route::get('/features', 'PetMetaController@features')->name('get.features');

            Route::get('/types', 'PetMetaController@types')->name('get.features');


        });

    });

});
